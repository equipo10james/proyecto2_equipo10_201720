package Tests;
import junit.framework.TestCase;
import model.data_structures.RojoNegro;


public class RojoNegroTest<Key extends Comparable<Key>, Value> extends TestCase
{
	private RojoNegro arbol;
	
	
	public void setup0()
	{
		arbol = new RojoNegro<>();
	}
	
	public void setup()
	{
		arbol = new RojoNegro<>();
		ClasePrueba cosas[] = new ClasePrueba[560];
		for (int i = 0; i < cosas.length; i++)
		{
			ClasePrueba var = new ClasePrueba("nombre" + i, i);
			cosas[i] = var;
		}
		
		for (int i = 0; i < cosas.length; i++)
		{
			ClasePrueba var = cosas[i];
			int llave = cosas[i].darCodigo();
			arbol.put(var.darCodigo(), var);
		}
	}
	
	public void testSize()
	{
		setup0();
		ClasePrueba cosa = new ClasePrueba("asdf", 5651);
		arbol.put(cosa.darCodigo(), cosa);
		assertEquals(1, arbol.size());
		setup();
		arbol.put(cosa.darCodigo(), cosa);
		assertEquals(561, arbol.size());
		
		arbol.deleteMax();
		assertEquals(560, arbol.size());
		
	}
	
	public void testContains()
	{

		setup();
		assertEquals(true, arbol.contains(15));
		assertEquals(false, arbol.contains(561));
	}
	
	public void testGet()
	{
		setup();
		ClasePrueba cosa = new ClasePrueba("asdf", 1000);
		arbol.put(cosa.darCodigo(), cosa);
		assertEquals(cosa, arbol.get(1000));
		arbol.delete(1000);
		assertEquals(null, arbol.get(1000 ));
	}
	
	
	
}
