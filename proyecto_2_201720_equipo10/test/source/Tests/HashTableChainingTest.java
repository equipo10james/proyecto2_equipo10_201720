package Tests;

import junit.framework.TestCase;
import model.data_structures.HashTableChaining;
import model.data_structures.HashTableProbing;

public class HashTableChainingTest <Key, Value> extends TestCase
{

	private HashTableChaining hashTable;
		
		private void setup() throws Exception 
		{
			hashTable = new HashTableChaining();
			
			ClasePrueba cosas[] = new ClasePrueba[30];
			for (int i = 0; i < cosas.length; i++)
			{
				ClasePrueba var = new ClasePrueba("nombre" + i, i);
				cosas[i] = var;
			}
			
			for (int i = 0; i < cosas.length; i++)
			{
				ClasePrueba var = cosas[i];
				int llave = cosas[i].darCodigo();
				hashTable.put(llave, var);
			}
		}
		
		public void setup1()
		{
			
			hashTable = new HashTableChaining<Key, ClasePrueba>();
		}
		public void testIsEmpty() throws Exception
		{
			setup();
			assertEquals(hashTable.isEmpty(), false);
			setup1();
			assertEquals(true, hashTable.isEmpty());
		}
		
		public void testNumCosas() throws Exception
		{
			setup();
			assertEquals(hashTable.numCosas(), 30);
			setup1();
			assertEquals(hashTable.numCosas(), 0);
		}
		
		public void testResize() throws Exception
		{
			setup();
			hashTable.resize(45);
			assertEquals(45, hashTable.tamano());
			setup1();
			assertEquals(hashTable.tamano(), 4);
		}
		public void testTamano() throws Exception
		{
			setup();
			assertEquals(8, hashTable.tamano());
			setup1();
			assertEquals(hashTable.tamano(), 4);
			
		
		}
		
		
		public void testContains() throws Exception
		{
			setup();
			assertEquals(true, hashTable.contains(15));
		}
		
		public void testGet() throws Exception
		{
			setup();
			ClasePrueba cosa = new ClasePrueba("hola", 50);
			hashTable.put(50, cosa);
			assertEquals(cosa, hashTable.get(50));
		}
		public void testDelete() throws Exception
		{
			setup();
			hashTable.delete(4);
			assertEquals(29, hashTable.numCosas());
		}
	}


