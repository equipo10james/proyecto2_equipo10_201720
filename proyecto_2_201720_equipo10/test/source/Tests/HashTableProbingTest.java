package Tests;

import junit.framework.TestCase;
import model.data_structures.HashTableProbing;

public class HashTableProbingTest<Key, Value> extends TestCase
{
	private HashTableProbing hashTable;
	
	private void setup() 
	{
		hashTable = new HashTableProbing<Key, ClasePrueba>();
		
		ClasePrueba cosas[] = new ClasePrueba[500];
		for (int i = 0; i < cosas.length; i++)
		{
			ClasePrueba var = new ClasePrueba("nombre" + i, i);
			cosas[i] = var;
		}
		
		for (int i = 0; i < cosas.length; i++)
		{
			ClasePrueba var = cosas[i];
			int llave = cosas[i].darCodigo();
			hashTable.put(llave, var);
		}
	}
	
	public void setup1()
	{
		
		hashTable = new HashTableProbing<Key, ClasePrueba>();
	}
	public void testIsEmpty()
	{
		setup();
		assertEquals(hashTable.isEmpty(), false);
		setup1();
		assertEquals(true, hashTable.isEmpty());
	}
	
	public void testNumCosas()
	{
		setup();
		assertEquals(hashTable.numCosas(), 500);
		setup1();
		assertEquals(hashTable.numCosas(), 0);
	}
	
	public void testResize()
	{
		setup();
		hashTable.resize(1000);
		assertEquals(1000, hashTable.tamano());
		setup1();
		assertEquals(hashTable.numCosas(), 0);
	}
	public void testTamano()
	{
		setup1();
		assertEquals(hashTable.tamano(), 4);
		setup();
		assertEquals(512, hashTable.tamano());
	
		
	
	}
	

	public void testContains() throws Exception
	{
		setup();
		assertEquals(true, hashTable.contains(15));
	}
	
	public void testGet() throws Exception
	{
		setup();
		ClasePrueba cosa = new ClasePrueba("hola", 50);
		hashTable.put(50, cosa);
		assertEquals(cosa, hashTable.get(50));
	}
	public void testDelete() throws Exception
	{
		setup();
		hashTable.delete(4);
		assertEquals(499, hashTable.numCosas());
	}
}