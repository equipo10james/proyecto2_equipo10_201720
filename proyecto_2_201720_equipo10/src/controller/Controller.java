package controller;

import api.ISTSManager;
import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.logic.STSManager;
import model.vo.RangoHora;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOTransbordo;
import model.vo.VOTrip;

public class Controller 
{
	/**
	 * modela el manejador de la clase logica
	 */
	private static ISTSManager manager  = new STSManager();
	private static String fecha;

	public static void ITSInit() 
	{
		manager.init();
	}


	public static void ITScargarGTFS() 
	{
		manager.cargarGTFS(); 
	}


	public static void ITScargarTR(String fechaCarga) 
	{
		fecha = fechaCarga;
		manager.cargarTR(fecha);
	}

	public static RingList<VOTrip> ITSviajesRetrasadosRuta(String idRuta) 
	{ 
		return manager.viajesRetrasadosRuta(idRuta, fecha);
	}


	public static RingList<VOStop> ITSparadasRetrasadasFecha(int n)
	{ 
		return manager.paradasRetrasadasFecha(n, fecha);
	}


	public static RingList<VOTransbordo> ITStransbordosRuta(String idRuta) 
	{ 
		return manager.ITStransbordosRuta(idRuta, fecha);
	}


	public static RingList<VOTrip> ITSviajesRetrasoTotalRuta(String idRuta)
	{
		return manager.viajesRetrasoTotalRuta(idRuta, fecha);
	}


	public static RangoHora ITSretardoHoraRuta(String idRuta)
	{ 
		return manager.ITSretardoHoraRuta(idRuta, fecha);
	}


	public static RingList<VOTrip> ITSbuscarViajesParadas(String idOrigen, String idDestino, String horaInicio,
			String horaFin) 
	{
		return manager.buscarViajesParadas(idOrigen, idDestino, fecha, horaInicio, horaFin);
	}


	public static RingList<VOTrip> ITSviajesMayorDistancia(int n)
	{
		return manager.viajesMayorDistancia(n, fecha);
	}


	public static RojoNegro<String, VORetraso> retardosViaje(String idViaje){

		return manager.retardosViaje(fecha, idViaje);
	}


	public static RingList<VORoute> ITSParadasCompartidas(String idStop)
	{
		return manager.paradaCompartida(fecha, idStop);
	}


	public static RingList<VOTrip> viajesRango(String idRuta, String horaInicio, String horaFin)
	{
		return manager.viajesRango(idRuta, horaInicio, horaFin);
	}

}
