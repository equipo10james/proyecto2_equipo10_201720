package model.logic;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;

import api.ISTSManager;
import model.data_structures.HashTableChaining;
import model.data_structures.HashTableProbing;
import model.data_structures.MaxPQ;
import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.vo.RangoHora;
import model.vo.Retornable;
import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOCalendar;
import model.vo.VOCalendarDates;
import model.vo.VOFeedInfo;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTransbordo;
import model.vo.VOTransfers;
import model.vo.VOTrip;

public class STSManager implements ISTSManager 
{

	//------------------------------------
	// ATRIBUTOS
	//------------------------------------

	private HashTableProbing<String, VORoute> rutasProbing;
	private HashTableChaining<String, VORoute> rutasChaining;

	private RingList<VOStopTimes> listaStopTimes;

	private HashTableChaining<String, VOTrip> viajesChaining;
	private RingList<VOTrip> listaViajes;
	private MaxPQ heapViajes;

	private RingList<VOStop> listaParadas;

	private HashTableChaining<String, VOStop> listaParadasChaining;


	private RingList<VOBusUpdate> actualizacionBus;


	private HashTableChaining<String, VOAgency> listaAgencias;

	private HashTableChaining<String, VOCalendar> calendarios;

	private RingList<VOCalendarDates> excepcionesCalendario;

	private RingList<VOFeedInfo> listaFeed;

	private RingList<VORetraso> listaRetrasos;

	private RingList<VOShape> listaShapes;

	private RingList<VOTransfers> listaTransfers;



	//------------------------------------
	// METODOS DE LEER LOS ARCHIVOS
	//------------------------------------

	public void readBusUpdate(File rtFile) 
	{
		actualizacionBus = new RingList<VOBusUpdate>();
		Gson gson = new Gson();

		try
		{
			VOBusUpdate[] info = gson.fromJson(new FileReader(rtFile), VOBusUpdate[].class);
			for (int i=0; i<info.length; i++ )
			{
				actualizacionBus.addFirst(info[i]);
			}

			VOBusUpdate[] buses= gson.fromJson(new FileReader(rtFile), VOBusUpdate[].class);
			for (int i = 0; i < buses.length; i++) 
			{
				VOBusUpdate currentBus= buses[i];
				actualizacionBus.addFirst(currentBus);

				String tripId = currentBus.trip_id()+"";

				VOTrip viajeActual = viajesChaining.get(tripId);

				if(viajeActual != null)
				{
					viajeActual.agregarUpdate(currentBus);
				}
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	//Carga las rutas tanto en tabla de hash probing como en chaining
	public void loadRoutes(String routesFile) 
	{
		try 
		{
			rutasChaining = new HashTableChaining<String, VORoute>(6007);
			rutasProbing = new HashTableProbing<String, VORoute>(3001);


			BufferedReader br = new BufferedReader(new FileReader(routesFile));

			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String route_color = null;
				String route_text_color = null;

				if(info.length==10)
				{
					route_color = info[7];

					route_text_color = info[8];
				}
				else
				{
					route_color="";

					route_text_color="";
				}


				VORoute ruta = new VORoute (info[0], info[1], info[2], info[3], info[4], info[5], info[6],  route_color, route_text_color);
				//System.out.println(ruta);


				rutasChaining.put(info[0], ruta);
				rutasProbing.put(info[0], ruta);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	//Carga los viajes tanto en lista como en tabla de hash chaining (con llave el tripid)
	public void loadTrips(String tripsFile) 
	{
		try 
		{
			listaViajes = new RingList<VOTrip>();
			viajesChaining = new HashTableChaining<String, VOTrip>(6007);

			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTrip viaje = new VOTrip (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				//System.out.println(viaje);

				//Carga el calendario del viaje
				VOCalendar calendario = calendarios.get(viaje.service_id());
				viaje.agregarCalendario(calendario);

				//Cargar viajes a rutas
				VORoute ruta = rutasChaining.get(viaje.route_id());

				if(rutasChaining.contains(viaje.route_id()))
					ruta.agregarViaje(viaje);

				listaViajes.addLast(viaje);
				viajesChaining.put(info[2], viaje);

				linea = br.readLine();
			}

			br.close();
		}
		/**
			for (int i = 0; i < listaRutas.size(); i++) 
			{
				for (int j = 0; j < listaViajes.size(); j++) 
				{
					if( ((VOTrip) listaViajes.getElement(j)).route_id().equals(((VORoute) listaRutas.getElement(i)).id()))
					{
						((VORoute) listaRutas.getElement(i)).agregarViaje( (VOTrip) listaViajes.getElement(j) );
					}
				}
			}

		} */

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	//Carga los stoptimes en una lista 
	public void loadStopTimes(String stopTimesFile) 
	{

		try 
		{
			listaStopTimes = new RingList<VOStopTimes>();

			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String shape = new String();

				if(info.length==8)
				{
					shape="";
				}
				else if (info.length==9)
				{
					shape = info[8];
				}


				VOStopTimes stopTime = new VOStopTimes (info[0], info[1], info[2], info[3], info[4],info[5], info[6], info[7], shape);

				VOTrip viaje = viajesChaining.get(stopTime.trip_id());

				//Agregar Viajes a Paradas en hash
				viaje.actualizarTiempoLlegada(stopTime.arrival_time());


				VOStop parada = listaParadasChaining.get(stopTime.stop_id());

				if(listaParadasChaining.contains(stopTime.stop_id()))
					parada.agregarViaje(viaje.trip_id());


				//Agregar stopTimes a Trip en hash
				viaje.agregarStopTimes(stopTime);



				//agrega parada a viaje
				viaje.agregarParada(parada);

				//System.out.println(stopTime);
				listaStopTimes.addLast(stopTime);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	//Carga las paradas tanto en una lista como en una tabla de hash con chaining
	public void loadStops(String stopsFile) 
	{
		try 
		{
			listaParadas = new RingList<VOStop>();
			listaParadasChaining = new HashTableChaining<String, VOStop>(6007);


			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStop parada = new VOStop (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], "");
				//System.out.println(parada);
				listaParadas.addLast(parada);
				listaParadasChaining.put(info[0], parada);

				linea = br.readLine();
			}

			br.close();

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	public void loadAgency(String agencyFiles)
	{
		try 
		{
			listaAgencias = new HashTableChaining<String, VOAgency>(6007);

			BufferedReader br = new BufferedReader(new FileReader(agencyFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOAgency agencia = new VOAgency (info[0], info[1], info[2], info[3], info[4]);
				//System.out.println(agencia);
				listaAgencias.put(info[0], agencia);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo agency");
	}

	public void loadCalendar(String calendarFiles)
	{
		try 
		{
			calendarios = new HashTableChaining<String, VOCalendar>(6007);

			BufferedReader br = new BufferedReader(new FileReader(calendarFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				int startDate = Integer.parseInt(info[8]);
				int endDate = Integer.parseInt(info[9]);

				VOCalendar calendario = new VOCalendar (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], startDate, endDate);
				//System.out.println(calendario);
				calendarios.put(info[0], calendario);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo calendar");
	}



	public void loadCalendar_dates(String calendar_datesFiles)
	{
		try 
		{
			excepcionesCalendario = new RingList<VOCalendarDates>();

			BufferedReader br = new BufferedReader(new FileReader(calendar_datesFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				VOCalendarDates calendarException = new VOCalendarDates(info[0], info[1], info[2]);
				//	System.out.println(calendarException);


				excepcionesCalendario.addLast(calendarException);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo exceptions");
	}

	public void loadShapes(String shapesFiles)
	{
		try 
		{
			listaShapes = new RingList<VOShape>();

			BufferedReader br = new BufferedReader(new FileReader(shapesFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOShape shape = new VOShape (info[0], info[1], info[2], info[3], info[4]);
				//System.out.println(shape);
				listaShapes.addLast(shape);


				linea = br.readLine();
			}


			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo shapes");

	}

	public void loadFeedInfo(String feed_infoFiles)
	{
		try 
		{
			listaFeed = new RingList<VOFeedInfo>();

			BufferedReader br = new BufferedReader(new FileReader(feed_infoFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOFeedInfo feed = new VOFeedInfo (info[0], info[1], info[2], info[3], info[4], info[5]);
				//System.out.println(feed);
				listaFeed.addLast(feed);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo feedinfo");
	}

	public void loadTransfers(String transfersFiles)
	{
		try 
		{
			listaTransfers = new RingList<VOTransfers>();

			BufferedReader br = new BufferedReader(new FileReader(transfersFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				String ultimo = "";

				if(info.length==4)
				{
					ultimo = info[3];
				}

				VOTransfers transfer = new VOTransfers (info[0], info[1], info[2], ultimo);
				//System.out.println(transfer);
				listaTransfers.addLast(transfer);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		// System.out.println("cargo transfers");
	}


	//------------------------------------
	// METODOS AUXILIARES DE PARADAS
	//------------------------------------


	//Se agrega a cada parada (de la tabla chaining) una lista con todos los TRIPID de los viajes de la parada. 
	public void agregarViajesAParada()
	{
		if(!listaStopTimes.isEmpty())
		{
			for (int i = 0; i < listaStopTimes.size(); i++) 
			{
				VOStopTimes actual = (VOStopTimes) listaStopTimes.getElement(i);

				VOTrip viaje = null;
				if( viajesChaining.contains(actual.trip_id()))
				{
					viaje = viajesChaining.get(actual.trip_id());

					viaje.actualizarTiempoLlegada(actual.arrival_time());
				}


				if(listaParadasChaining.contains(actual.stop_id()))
				{
					(listaParadasChaining.get(actual.stop_id())).agregarViaje(viaje.trip_id());
				}

			}
		}
	}


	//Recorre las paradas de todos los viajes en una fecha dada para guardar todas las paradas de la ruta en la fecha
	public HashTableChaining<String, VOStop> darParadasRutaEnFecha(String idRuta, String fecha)
	{
		RingList<VOTrip> viajesEnFecha = darViajesFecha(fecha);
		HashTableChaining<String, VOStop> paradas = new HashTableChaining<String, VOStop>(6007);

		for (int i = 0; i < viajesEnFecha.size(); i++) 
		{
			if(((VOTrip) viajesEnFecha.getElement(i)).route_id().equals(idRuta))
			{

				RingList<VOStop> paradasViaje = ((VOTrip) viajesEnFecha.getElement(i)).darParadas();

				for (int j = 0; j < paradasViaje.size(); j++) 
				{
					if(!paradas.contains( ((VOStop) paradasViaje.getElement(j)).id() ))
					{
						paradas.put(((VOStop) paradasViaje.getElement(j)).id(), (VOStop) paradasViaje.getElement(j));
					}

				}
			}
		}

		return paradas;
	}


	//------------------------------------
	// METODOS AUXILIARES DE VIAJES
	//------------------------------------


	public void cargarInfoDeViaje(VOTrip viaje)
	{

		for (int i = 0; i < listaStopTimes.size(); i++) 
		{

			VOStopTimes actual = ((VOStopTimes) listaStopTimes.getElement(i));

			if(actual.trip_id().equals(viaje.trip_id()))
			{
				viaje.agregarStopTimes(actual);


				for (int j = 0; j < listaParadas.size(); j++) 
				{
					if(((VOStop) listaParadas.getElement(j)).id().equals(actual.stop_id()))
					{
						viaje.agregarParada(((VOStop) listaParadas.getElement(j)));
					}
				}
			}
		}
	}

	//Agrega los stoptimes y las paradas a todos los viajes de la tabla de hash de viajes
	public void agregarStopTimesYParadasAViajesChaining() 
	{
		Iterator<String> iterator =  viajesChaining.keys();

		while(iterator.hasNext())
		{
			String llaveActualViaje = iterator.next();

			VOTrip viajeActual = viajesChaining.get(llaveActualViaje);

			for (int i = 0; i < listaStopTimes.size(); i++) 
			{
				VOStopTimes actual = (VOStopTimes) listaStopTimes.getElement(i);

				if(actual.trip_id().equals(viajeActual.trip_id()))
				{
					viajeActual.agregarStopTimes(actual);


					Iterator<String> iterator1 =  listaParadasChaining.keys();

					while(iterator1.hasNext())
					{
						String llaveParada = iterator1.next();

						VOStop paradaActual = listaParadasChaining.get(llaveParada);

						if(paradaActual.id().equals(actual.stop_id()))
						{
							viajeActual.agregarParada(paradaActual);
						}
					}
				}
			}
		}

		System.out.println("acabo");
	}


	//Agrega los stoptimes y las paradas a todos los viajes de la lista de viajes
	public void agregarStopTimesYParadasAViajesLista() 
	{

		for (int j = 0; j < listaViajes.size(); j++) 
		{

			VOTrip viajeActual = (VOTrip) listaViajes.getElement(j);

			for (int i = 0; i < listaStopTimes.size(); i++) 
			{
				VOStopTimes actual = (VOStopTimes) listaStopTimes.getElement(i);
				System.out.println(actual);

				if(actual.trip_id().equals(viajeActual.trip_id()))
				{
					viajeActual.agregarStopTimes(actual);


					Iterator<String> iterator1 =  listaParadasChaining.keys();

					while(iterator1.hasNext())
					{
						String llaveParada = iterator1.next();

						VOStop paradaActual = listaParadasChaining.get(llaveParada);

						if(paradaActual.id().equals(actual.stop_id()))
						{
							viajeActual.agregarParada(paradaActual);
						}
					}
				}
			}
		}

	}

	//Carga los shapes, el calendario y las excepciones para cada viaje en la tabla de hash chaining
	public void agregarInfoExtraViajesChaining()
	{
		System.out.println("entro");
		Iterator<String> iterator =  viajesChaining.keys();
		System.out.println("iterador");

		while(iterator.hasNext())
		{
			VOTrip viajeActual = viajesChaining.get(iterator.next());

			System.out.println(viajeActual);
			//			//Carga Shapes
			//			Iterator<VOShape> iterator1 = listaShapes.iterator();
			//
			//			while(iterator1.hasNext())
			//			{
			//				VOShape shapeActual = iterator1.next();
			//
			//				if(viajeActual.shape_id().equals(shapeActual.shape_id()))
			//				{
			//					viajeActual.agregarShape(shapeActual);
			//				}
			//			}


			//			//Carga Exepciones de fecha
			//			Iterator<VOCalendarDates> iterator2 = excepcionesCalendario.iterator();
			//
			//			while(iterator2.hasNext())
			//			{
			//				VOCalendarDates exceptionActual = iterator2.next();
			//
			//				if(viajeActual.service_id().equals(exceptionActual.service_id()))
			//				{
			//					viajeActual.agregarCalendarException(exceptionActual);
			//				}
			//			}
		}

		System.out.println("cargo info extra chaining");
	}


	//Carga los shapes, el calendario y las excepciones para cada viaje en la lista
	public void agregarInfoExtraViajesLista()
	{

		for (int j = 0; j < listaViajes.size(); j++) 
		{

			VOTrip viajeActual = (VOTrip) listaViajes.getElement(j);

			//Carga Shapes

			for (int i = 0; i < listaShapes.size(); i++) 
			{
				VOShape shapeActual = (VOShape) listaShapes.getElement(i);

				if(viajeActual.shape_id().equals(shapeActual.shape_id()))
				{
					viajeActual.agregarShape(shapeActual);
				}
			}



			//Carga Exepciones de fecha

			for (int i = 0; i < excepcionesCalendario.size(); i++) 
			{
				VOCalendarDates exceptionActual = (VOCalendarDates) excepcionesCalendario.getElement(i);

				if(viajeActual.service_id().equals(exceptionActual.service_id()))
				{
					viajeActual.agregarCalendarException(exceptionActual);
				}
			}
		}
	}


	//pFecha se da en formato YYYYMMDD y se retorna si hay o no una excepcion para esa fecha en el viaje dado
	public boolean fechaConExcepcion(String pFecha, String serviceId)
	{
		boolean rep = false;

		for (int i = 0; i < excepcionesCalendario.size(); i++)
		{
			VOCalendarDates exceptionActual = (VOCalendarDates) excepcionesCalendario.getElement(i);

			if(pFecha.equals( exceptionActual.date()) && exceptionActual.service_id().equals(serviceId))
			{
				rep = true;
			}
		}

		return rep;
	}


	//pFecha se da en formato YYYYMMDD y se retornan todos los viajes en dicha fecha
	public RingList<VOTrip> darViajesFecha(String pFecha)
	{
		RingList<VOTrip> viajesEnFecha = new RingList<VOTrip>();
		RingList<String> services = new RingList<String>();

		Iterator <String> iterador1 = calendarios.keys();

		while (iterador1.hasNext())
		{
			VOCalendar calendarActual = calendarios.get(iterador1.next());

			int fecha = Integer.parseInt(pFecha);

			if( calendarActual.start_date() < fecha && calendarActual.end_date() > fecha)
			{
				String dia = darDia(pFecha);

				if(dia.equalsIgnoreCase("lunes") && Integer.parseInt(calendarActual.monday()) == 1 )
				{

					services.addLast(calendarActual.service_id());

				}
				else if(dia.equalsIgnoreCase("martes") && Integer.parseInt(calendarActual.tuesday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("miercoles") && Integer.parseInt(calendarActual.wednesday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("jueves") && Integer.parseInt(calendarActual.thursday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("viernes") && Integer.parseInt(calendarActual.friday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("sabado") && Integer.parseInt(calendarActual.saturday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("domingo") && Integer.parseInt(calendarActual.sunday()) == 1 )
				{

					services.addLast(calendarActual.service_id());
				}
			}
		}

		Iterator<String> iterador = viajesChaining.keys();
		//		Iterator<String> iterador2 = services.iterator();
		//		System.out.println("iterador servicios");

		for (int i = 0; i < services.size(); i++) 
		{
			String serviceId  = (String) services.getElement(i);

			while(iterador.hasNext())
			{
				VOTrip viaje = viajesChaining.get(iterador.next());

				if(viaje.service_id().equals(serviceId))
				{
					System.out.println(viaje);
					viajesEnFecha.addLast(viaje);
				}

			}
		}



		return viajesEnFecha;
	}



	//pFecha se da en formato YYYYMMDD y retorna el dia de esa fecha
	private String darDia(String pFecha)
	{
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
		String dia = null;

		try 
		{
			Date fecha = formato.parse(pFecha);
			DateFormat formato1 = new SimpleDateFormat("EEEE");
			dia = formato1.format(fecha);
		} 

		catch (ParseException e) 
		{
			e.printStackTrace();
		}

		return dia;
	}


	//------------------------------------
	// METODOS AUXILIARES DE RUTAS
	//------------------------------------

	// Recorre los viajes y los agrega a la ruta corespondiente (en la tabla probing y chaining)
	public void cargarViajesARutas()
	{

		for (int i = 0; i < listaViajes.size(); i++) 
		{
			VOTrip viajeActual = (VOTrip) listaViajes.getElement(i);

			rutasProbing.get(viajeActual.route_id()).agregarViaje(viajeActual);
			rutasChaining.get(viajeActual.route_id()).agregarViaje(viajeActual);
		}
	}


	//Retorna todas las paradas con sus respectivas rutas para una fecha dada
	public HashTableChaining<VOStop, RingList<VORoute>>  rutasQuePasanParadasEnFecha(String fecha)
	{
		HashTableChaining<VOStop, RingList<VORoute>> rutasParadas = new HashTableChaining<VOStop, RingList<VORoute>>();

		RingList<VOTrip> viajesFecha = darViajesFecha(fecha);


		for (int i = 0; i < viajesFecha.size(); i++) 
		{
			VOTrip viajeActual = (VOTrip) viajesFecha.getElement(i);
			VORoute ruta = null;

			ruta = rutasChaining.get(viajeActual.route_id());


			RingList<VOStop> paradasViaje = viajeActual.darParadas();


			for (int j = 0; j < paradasViaje.size(); j++) 
			{
				VOStop paradaActual = (VOStop) paradasViaje.getElement(j);

				if(!rutasParadas.contains(paradaActual))
				{
					RingList<VORoute> rutas = new RingList<VORoute>();
					rutas.addFirst(ruta);

					rutasParadas.put(paradaActual, rutas);
				}

				else
				{
					rutasParadas.get(paradaActual).addLast(ruta);

				}	
			}
		}


		return rutasParadas;
	}


	//------------------------------------
	// METODOS DE INFORMACION DE UPDATES
	//------------------------------------

	//Retorna todas las actualizaciones de un viaje
	public RingList<VOBusUpdate> darInfoDeBusViaje(String tripId)
	{
		RingList<VOBusUpdate> actualizacionDeBus = new RingList<VOBusUpdate>();

		for (int i = 0; i < actualizacionBus.size(); i++) 
		{
			VOBusUpdate cosa = (VOBusUpdate) actualizacionBus.getElement(i);
			if(cosa.trip_id().equals(tripId))
			{
				actualizacionDeBus.addFirst(cosa);
			}
		}

		return actualizacionDeBus;

	}


	//------------------------------------
	// METODOS DE RETRASOS
	//------------------------------------

	//Carga los retrasos de todos los viajes en la lista de retrasos
	public void cargarRetrasosLista()
	{
		RingList<VORetraso> retrasos = new RingList<VORetraso>();


		for (int i = 0; i < listaViajes.size(); i++) 
		{
			VOTrip viajeActual = (VOTrip) listaViajes.getElement(i);
			RingList<VORetraso> retrasosViaje = viajeActual.darRetrasos();


			for (int j = 0; j < retrasosViaje.size(); j++) 
			{
				retrasos.addLast((VORetraso) retrasosViaje.getElement(j));
			}
		}

		listaRetrasos = retrasos;
		System.out.println("cargo retrasos lista");

	}

	//Carga los retrasos de cada viaje en las dos estructuras
	public void cargarRetrasosViajes()
	{
		//Carga los retrasos de los viajes del chaining

		Iterator<String> iterador = viajesChaining.keys();

		while(iterador.hasNext())
		{
			VOTrip actual = viajesChaining.get(iterador.next());

			encontrarRetardos(actual);

		}

		System.out.println("cargo retrasos viajes");
	}


	//Encuentra los retrasos de el viaje ingresado por parametro
	public void encontrarRetardos(VOTrip viaje)
	{
		if(viaje != null)
		{
			//Buscar actualizaciones del bus para el viaje ingresado por parametro
			RingList<VOBusUpdate> actualizaciones = viaje.darUpdatesViajes();

			//Lista de los retrasos de viaje
			RingList<VORetraso> retrasos  = new RingList<VORetraso>();

			RingList<VOStop> paradasViaje = viaje.darParadas();
			RingList<VOStopTimes> stopTimesViaje = viaje.darStopTimes();

			//			Iterator<VOStop> iterador1 = paradasViaje.iterator();
			//			Iterator <VOStopTimes> iterador2 = stopTimesViaje.iterator();

			VORoute ruta = rutasChaining.get(viaje.route_id());


			for (int i = 0; i < paradasViaje.size(); i++) 
			{
				VOStop stopActual = (VOStop) paradasViaje.getElement(i);
				VOStopTimes stopTimeDelStop = (VOStopTimes) stopTimesViaje.getElement(i);

				double lat = Double.parseDouble(stopActual.lat());
				double lon = Double.parseDouble(stopActual.lon());



				VOBusUpdate menorCercano = null;
				VOBusUpdate mMenorCercano = null;

				double menor = Double.MAX_VALUE;
				double mMenor = Double.MAX_VALUE;


				for (int j = 0; j < actualizaciones.size(); j++) 
				{
					VOBusUpdate actual = (VOBusUpdate) actualizaciones.getElement(j);
					double latComp = Double.parseDouble(actual.latitude());
					double lonComp = Double.parseDouble(actual.longitude());
					double distancia = getDistance(lat, lon, latComp, lonComp);

					if(distancia<mMenor)
					{
						menorCercano = mMenorCercano;
						menor = mMenor;
						mMenorCercano = actual;
						mMenor = distancia;
					}

					else if (distancia<menor)
					{
						menorCercano = actual;
						menor = distancia;
					}
				}

				double retardo = 0;

				if(menorCercano != null && mMenorCercano!= null)
				{

					String horaBus1 = menorCercano.recordedTime();
					String horaBus2 = mMenorCercano.recordedTime();


					int hora1 = horaAsegundos(horaBus1);
					int hora2 = horaAsegundos(horaBus2);


					int promedio = (hora1 + hora2)/2;

					int horaStop = horaAsegundos(stopTimeDelStop.departure_time());

					retardo = horaStop-promedio;

				}

				if(retardo < 0)
				{
					VORetraso retraso = new VORetraso(stopActual, viaje, ruta, viaje.darCalendar().start_date()+"", retardo);
					retrasos.addLast(retraso);
				}

			}

			viaje.cargarRetrasos(retrasos);
		}
	}



	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final double R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;

		return distance;

	}

	private double toRad( double value)
	{
		return (value * Math.PI / 180);
	}


	//De formato HH:MM:SS o HH:MM:SS TT donde TT es o "pm" o "am" a int con segundos
	public int horaAsegundos(String hora)
	{
		int rta = 0;

		String[] arreglo= hora.split(":");

		String[] segundosAMPM = arreglo[2].split(" ");

		rta = ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(segundosAMPM[0])));

		if(segundosAMPM[1].equalsIgnoreCase("pm"))
		{
			rta += (12*3600);
		}

		return rta;
	}


	//------------------------------------
	// METODOS PARA CARGAR INFORMACION
	//------------------------------------


	@Override
	public void cargarGTFS() 
	{
		loadAgency("./data/agency.txt");
		System.out.println("cargo agencias");
		loadCalendar("./data/calendar.txt");
		System.out.println("cargo calendarios");
		loadCalendar_dates("./data/calendar_dates.txt");
		System.out.println("cargo exceptions");
		loadShapes("./data/shapes.txt");
		System.out.println("cargo shapes");
		loadFeedInfo("./data/feed_info.txt");
		System.out.println("cargo feedinfo");
		loadTransfers("./data/transfers.txt");
		System.out.println("cargo transfers");

		loadRoutes("./data/routes.txt");
		System.out.println("cargo ruta");

		loadTrips("./data/trips.txt");
		System.out.println("cargo viajes");

		loadStops("./data/stops.txt");
		System.out.println("cargo stops");

		loadStopTimes("./data/stop_times.txt");
		System.out.println("cargo stopstimes");

		System.out.println("cargo estatico");
	}

	//Fecha entra en formato AAAAMMDD
	@Override
	public void cargarTR(String fecha) 
	{
		String mes = fecha.substring(5, 6);
		String dia = fecha.substring(6);


		String fechaFinal = mes+"-"+dia; 

		File archivo1 = new File("./data/RealTime-"+fechaFinal+"-BUSES_SERVICE");
		File[] files1 = archivo1.listFiles();


		for (int i = 0; i < files1.length; i++) 
		{
			readBusUpdate(files1[i]);
		}
		System.out.println("cargo temp");

	}



	//------------------------------------
	// METODOS REQUERIMIENTOS
	//------------------------------------


	//Punto 1A
	@Override
	public RingList<VOTrip> viajesRetrasadosRuta(String idRuta, String fecha) 
	{
		System.out.println("entro");
		RingList<VOTrip> respuesta = new RingList<VOTrip>();

		VORoute ruta = rutasProbing.get(idRuta);

		RingList<VOTrip> viajesFecha = darViajesFecha(fecha);
		System.out.println("llego aqui");

		if(viajesFecha.size()!=0)
		{
			for (int i = 0; i < viajesFecha.size(); i++) 
			{
				VOTrip viajeActual = (VOTrip) viajesFecha.getElement(i);
				System.out.println(viajeActual);

				if(viajeActual.route_id().equals(ruta.id()) && viajeActual.darRetrasos().size()>0)
				{

					//ordenar paradas de viaje por tiempo de retraso
					viajeActual.ordenarParadasPorTiempoRetraso();
					respuesta.addFirst(viajeActual);
				}
			}
		}
		System.out.println("salio");

		//Ordeno los viajes por id de viaje en orden descendente
		respuesta.sort(1);

		return respuesta;

	}


	//Punto 2A
	@Override
	public RingList<VOStop> paradasRetrasadasFecha(int n, String fecha) 
	{
		RingList<VOStop> paradasRetrasadasFecha = new RingList<VOStop>();
		RingList<VOTrip> viajesEnFecha = darViajesFecha(fecha);

		HashTableChaining<String, VOStop> paradasEnChaining = new HashTableChaining< String, VOStop>();
		MaxPQ<VOStop> heapParadas = new MaxPQ<VOStop>(listaParadas.size());

		if(viajesEnFecha.size() != 0)
		{

			for (int i = 0; i < viajesEnFecha.size(); i++) 
			{
				VOTrip viajeActual = (VOTrip) viajesEnFecha.getElement(i);

				if (darInfoDeBusViaje(viajeActual.trip_id()).size()!=0)
				{

					RingList<VORetraso> retrasosViaje = viajeActual.darRetrasos();

					if(retrasosViaje.size()!=0)
					{
						for (int j = 0; j < retrasosViaje.size(); j++) 
						{
							VORetraso retrasoActual = (VORetraso) retrasosViaje.getElement(j);

							VOStop parada = retrasoActual.parada();
							System.out.println(parada);

							if (paradasEnChaining.contains(parada.id()))
							{
								paradasEnChaining.get(parada.id()).agregarNumRetraso();
							}

							else 
							{
								paradasEnChaining.put(parada.id(), parada);
								parada.agregarNumRetraso(); 
							}
						}
					}
				} 
			}
		}


		Iterator<String> iterator3 = paradasEnChaining.keys();

		while(!paradasEnChaining.isEmpty() && iterator3.hasNext())
		{
			VOStop paradaActual = paradasEnChaining.get(iterator3.next());
			heapParadas.insert(paradaActual);
			System.out.println(paradaActual);
		}

		int contador = 0;

		while(!paradasEnChaining.isEmpty() && contador < n)
		{
			VOStop paradaConMasRetardos = heapParadas.max();

			paradasRetrasadasFecha.addLast(paradaConMasRetardos); 
			heapParadas.delMax();
			contador ++;
		}

		return paradasRetrasadasFecha;
	}


	//Punto 3A
	@Override
	public RingList<VOTransbordo> ITStransbordosRuta(String idRuta, String fecha) 
	{

		RingList<VOTransbordo> transbordos = new RingList<VOTransbordo>();

		//Ruta principal del id ingresado por parametro
		VORoute ruta = rutasChaining.get(idRuta);

		//Todas las paradas en la fecha dada por parametro de ruta principal
		HashTableChaining<String, VOStop> paradasRutaPrincipal = darParadasRutaEnFecha(idRuta, fecha);
		//HashTableChaining<VOStop, RingList<VORoute>> rutasDeParadas = rutasQuePasanParadasEnFecha(fecha);

		if(paradasRutaPrincipal.size()!=0)
		{
			Iterator<String> iterator = paradasRutaPrincipal.keys();


			//Recorre todas las paradas de la ruta principal
			while(iterator.hasNext())
			{
				VOStop paradaActual = paradasRutaPrincipal.get(iterator.next());
				VOTransbordo transbordo = new VOTransbordo();

				transbordo.agregarParadas(recur(paradaActual, ruta, fecha));


				transbordos.addLast(transbordo);

			}
		}

		return transbordos;

	}



	//Atributos de siguientes paradas
	private int siguientes = 0;



	// Metodo recursivo auxiliar Punto 3A
	public RingList<VOStop> recur(VOStop parada, VORoute ruta, String fecha)
	{
		//VOTransbordo transbordo = new VOTransbordo(); 

		HashTableChaining<VOStop, RingList<VORoute>> rutasDeParadas = rutasQuePasanParadasEnFecha(fecha);
		RingList<VORoute> rutasDeParada = rutasDeParadas.get(parada);

		RingList<VOStop> paradasTransbordo = new RingList<VOStop>();

		//Caso que sale de la recursion: si por la parada solo pasa la ruta entrada por parametro
		if(rutasDeParada.size() == 1)
		{
			parada.marcarPaso();
			paradasTransbordo.addLast(parada);

		}


		//Recursi�n: en el caso en el que por la parada pasen mas de una ruta
		else if(rutasDeParada.size() > 1)
		{
			parada.marcarPaso();

			//Agrega la parada de la que se desprenden los transbordos
			paradasTransbordo.addLast(parada);


			for (int i = 0; i < rutasDeParada.size(); i++)
			{
				VORoute rutaActual = (VORoute) rutasDeParada.getElement(i);

				if(!rutaActual.equals(ruta))
				{
					//Paradas de la ruta actual en la fecha
					HashTableChaining<String, VOStop> paradasRuta = darParadasRutaEnFecha(rutaActual.id(), fecha);

					System.out.println(paradasRuta);
					//Tengo que recorrer las paradas que van despues de parada para cada nueva ruta

					boolean llegoParada = false;

					Iterator<String> iterador1 = paradasRuta.keys();

					while(iterador1.hasNext())
					{
						VOStop paradaActual = paradasRuta.get(iterador1.next());

						if(paradaActual.equals(parada) &&!llegoParada)
						{
							llegoParada = true;
						}

						if(llegoParada == true && !paradaActual.darMarcador())
						{
							recur(paradaActual, rutaActual, fecha);
						}
					}

				}
			}

		}
		return paradasTransbordo;
	}



	///////////////////////////////////////////////////////////////////////////////////////

	//Punto 1B
	@Override
	public RingList<VOTrip> viajesRetrasoTotalRuta(String idRuta, String fecha) 
	{
		RingList<VOTrip> viajesRuta = new RingList<>();
		if(rutasChaining.get(idRuta)!= null)
		{
		 viajesRuta = rutasChaining.get(idRuta).darViajes();			

		}
		RingList viajesFecha = darViajesFecha(fecha);
		//saca los viajes deacuerdo al idRuta de param
		for (int i = 0; i <  viajesRuta.size(); i++)
		{
			VOTrip viajei = (VOTrip) viajesRuta.getNode(i).getElement();
			if (!viajesFecha.contains(viajei))
			{
				viajesRuta.removePos(i); // sca los que no esten en la fecha

			}
		}
		for (int i = 0; i < viajesRuta.size(); i++)
		{
			VOTrip viajei = (VOTrip) viajesRuta.getNode(i).getElement();


			for (int j = 0; j < viajei.darParadas().size(); j++)
			{
				boolean huboRetraso = false;    // recorre las paradas para todos los viajes
				// inicia la variable para recorrer las paradas,
				//si encuentra un retraso pone la variable en true y recorre el resto para ver si todas las paradas tienen retrasos
				VOStop paradaj = ((VOStop) viajei.darParadas().getNode(j).getElement());
				if(paradaj.huboRetraso())
				{
					huboRetraso = true;
					for (int k = j+1; k < viajei.darParadas().size() && huboRetraso; k++)
					{
						VOStop paradak = ((VOStop) viajei.darParadas().getNode(k).getElement());
						if(!paradak.huboRetraso())
						{
							huboRetraso = false;
						}
					}
				}
				if(!huboRetraso)
				{
					viajesRuta.removePos(j);	// si no todas tiene retrasos se borra de la lista
				}
			}

		}
		return viajesRuta;

	}

	//Punto 2B
	@Override
	public RangoHora ITSretardoHoraRuta(String idRuta, String Fecha) {
		VORoute ruta = rutasChaining.get(idRuta);
		RingList viajesRuta = ruta.darViajes();
		RingList viajesFecha = darViajesFecha(Fecha);
		RingList rangosHora = darRangoshora();	
		if (viajesRuta!= null)
		{
			for (int i = 0; i < viajesRuta.size(); i++)
			{
				if(!viajesFecha.contains(viajesRuta.getElement(i)))  		//sacan los viajes que no esten en la feca
				{
					viajesRuta.removePos(i);
				}
			}
		}
		
if(rangosHora!= null) {
	ruta.definirRangos(rangosHora);

	for (int i = 0; i < viajesRuta.size(); i++)
	{												//para cada rango revisa si el viaje 
		for (int j = 0; j < rangosHora.size(); j++) 
		{
			if(((RangoHora) rangosHora.getElement(j)).estaEnRango(((VOTrip) viajesRuta.getElement(i)).darTiempoLlegada()));
			{
				RingList retrasos = ((VOTrip)viajesRuta.getElement(i)).darRetrasos();
				((RangoHora) rangosHora.getElement(j)).agregarRetrasos(retrasos);
			}
		}
	}
}
		
		ruta.definirRangos(rangosHora);
		MaxPQ rangos = ruta.darRangosHora();
		RangoHora rango = (RangoHora) rangos.max();
		rango = (RangoHora) rangos.max();


		return rango;
	}

	public int queRangoEs(VOTrip trip)
	{
		int rta =0;
		int hora = horaAsegundos(trip.darTiempoLlegada());


		return rta;
	}

	public RingList darRangoshora()
	{
		RingList lista = new RingList();
		String string0  = "00:00:00";
		String string1  = "01:00:00";
		String string2  = "02:00:00";
		String string3  = "03:00:00";
		String string4  = "04:00:00";
		String string5  = "05:00:00";
		String string6  = "06:00:00";
		String string7  = "07:00:00";
		String string8  = "08:00:00";
		String string9  = "09:00:00";
		String string10  = "10:00:00";
		String string11  = "11:00:00";
		String string12  = "12:00:00";
		String string13  = "13:00:00";
		String string14  = "14:00:00";
		String string15  = "15:00:00";
		String string16  = "16:00:00";
		String string17  = "17:00:00";
		String string18  = "18:00:00";
		String string19  = "19:00:00";
		String string20  = "20:00:00";
		String string21  = "21:00:00";
		String string22  = "22:00:00";
		String string23  = "23:00:00";
		String string24  = "24:00:00";
		RangoHora f0 = new RangoHora(string0,string1);
		lista.addLast(f0);
		RangoHora f1 = new RangoHora(string1,string2);
		lista.addLast(f1);
		RangoHora f2 = new RangoHora(string2,string3);
		lista.addLast(f2);
		RangoHora f3 = new RangoHora(string3,string4);
		lista.addLast(f3);
		RangoHora f4 = new RangoHora(string4,string5);
		lista.addLast(f4);
		RangoHora f5 = new RangoHora(string5,string6);
		lista.addLast(f5);
		RangoHora f6 = new RangoHora(string6,string7);
		lista.addLast(f6);
		RangoHora f7 = new RangoHora(string7,string8);
		lista.addLast(f7);
		RangoHora f8 = new RangoHora(string8,string9);
		lista.addLast(f8);
		RangoHora f9 = new RangoHora(string9,string10);
		lista.addLast(f9);
		RangoHora f10 = new RangoHora(string10,string11);
		lista.addLast(f10);
		RangoHora f11 = new RangoHora(string11,string12);
		lista.addLast(f11);
		RangoHora f12 = new RangoHora(string12,string13);
		lista.addLast(f12);
		RangoHora f13 = new RangoHora(string13,string14);
		lista.addLast(f13);
		RangoHora f14 = new RangoHora(string14,string15);
		lista.addLast(f14);
		RangoHora f15 = new RangoHora(string15,string16);
		lista.addLast(f15);
		RangoHora f16 = new RangoHora(string16,string17);
		lista.addLast(f16);
		RangoHora f17 = new RangoHora(string17,string18);
		lista.addLast(f17);
		RangoHora f18 = new RangoHora(string18,string19);
		lista.addLast(f18);
		RangoHora f19 = new RangoHora(string19,string20);
		lista.addLast(f19);
		RangoHora f20 = new RangoHora(string20,string21);
		lista.addLast(f20);
		RangoHora f21 = new RangoHora(string21,string22);
		lista.addLast(f21);
		RangoHora f22 = new RangoHora(string22,string23);
		lista.addLast(f22);
		RangoHora f23= new RangoHora(string23,string24);
		lista.addLast(f23);

		return lista;
	}

	//Punto 3B
	@Override
	public RingList<VOTrip> buscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) {
		RingList viajesOrg = new RingList();
		if(listaParadasChaining.get(idOrigen)!= null)
		{
			viajesOrg = listaParadasChaining.get(idOrigen).darViajes();  //viajes de la parada Origen

		}
		RingList<VOTrip> rta = new RingList<VOTrip>();
		for (int i = 0; i < viajesOrg.size(); i++)
		{
			for (int j = 0; j < ((VOTrip) viajesOrg.getElement(i)).darParadas().size(); j++)
			{																		// Se mira por cada viaje que pase por el Origen sus paradas para ver si esta la parada final y si esta anade ese viaje a la respuesta
				VOStop paradaAux =(VOStop) ((VOTrip) viajesOrg.getElement(i)).darParadas().getElement(j);
				if(paradaAux != null)
				{
					if(paradaAux.id().equals(idDestino))
					{
						rta.addFirst((VOTrip) viajesOrg.getElement(i));
					}
				}
				
			}
		}
		RingList viajesFecha = darViajesFecha(fecha);
		for (int i = 0; i < rta.size(); i++) 
		{
			if(!viajesFecha.contains(rta.getElement(i)) && rta.getElement(i)!= null)		// reviso que este en la fecha
				rta.removePos(i);
		}

		HashTableChaining<String, String> tripsIDenHora = new HashTableChaining<>();
		for (int i = 0; i < listaStopTimes.size(); i++) 
		{

			if(estaEntre((VOStopTimes) listaStopTimes.getElement(i), horaInicio, horaFin) )
			{
				tripsIDenHora.put((((VOStopTimes) listaStopTimes.getElement(i)).trip_id()), (((VOStopTimes) listaStopTimes.getElement(i)).trip_id()));
			}
		}

		for (int i = 0; i < rta.size(); i++)
		{
			if (!tripsIDenHora.contains(((VORoute) rta.getElement(i)).id())&& rta.getElement(i)!=null)
			{
				rta.removePos(i);
			}
		}

		return rta;
	}


	public boolean estaEntre(VOStopTimes stopTime, String horaInicio, String horaFinal)
	{
		boolean rta = false;
		int horaIn = horaAsegundos(horaInicio);
		int horaFin = horaAsegundos(horaFinal);
		int departureTime = horaAsegundos(stopTime.departure_time());
		if(horaIn <= departureTime && departureTime <= horaFin)
		{
			rta = true;
		}

		return rta;		
	}

	///////////////////////////////////////////////////////////////////////////////////////


	//Punto 1C
	@Override
	public void init() 
	{
		//		agregarViajesAParada();
		//		System.out.println("cargo viajes a Parada");
		//
		//		agregarStopTimesYParadasAViajesLista();
		//		System.out.println("cargo a viajes stoptime y parada lista");
		//
		//		agregarStopTimesYParadasAViajesChaining();
		//		System.out.println("cargo a viajes stoptime y parada chaining");
		//
		//		agregarInfoExtraViajesChaining();
		//		System.out.println("cargo a viajes info extra chaining");
		//
		//		agregarInfoExtraViajesLista();
		//		System.out.println("cargo viajes info extra lista");
		//
		//		cargarViajesARutas();
		//		System.out.println("cargo viajes a rutas");

		cargarRetrasosViajes();


		cargarRetrasosLista();

	}

	//Punto 2C
	@Override
	public RingList<VOTrip> viajesMayorDistancia(int n, String fecha) 
	{
		RingList viajesFecha = darViajesFecha(fecha);

		MaxPQ var = new MaxPQ<>(viajesChaining.size());		

		if(viajesChaining.size()!=0)
		{
			Iterator iterator = viajesChaining.keys();

			while(iterator.hasNext())
			{
				VOTrip viajeAct = (VOTrip) iterator.next();
				var.insert(viajeAct);
			}
		}

		RingList<VOTrip> rta = new RingList<VOTrip>();
		for (int i = 0; i < n; i++) 
		{
			while(!viajesFecha.contains( var.max())) //hace n veces: borra hasta encontrar el que este en la fecha y luego lo mete enla respuesta
			{
				var.delMax();
			}

			rta.addFirst((VOTrip) var.max());
		}
		return rta;
	}   

	//Punto 3C
	@Override
	public RojoNegro<String, VORetraso> retardosViaje(String fecha, String idViaje) {

		RingList retrasosViaje = new RingList();
		if(viajesChaining.size()!=0)
		{
			retrasosViaje = viajesChaining.get(idViaje).darParadas();
		}

		RojoNegro rta = new RojoNegro();
		for (int i = 0; i < retrasosViaje.size(); i++)
		{
			rta.put( ((VORetraso) retrasosViaje.getElement(i)).darSegundosRetrasados(), retrasosViaje.getElement(i));
		}



		return rta;

	}

	//Punto 4C
	@Override
	public RingList<VORoute> paradaCompartida(String fecha, String idParada) 
	{
		RojoNegro<String, Retornable> paradasCompartidas = darParadasCompartidas(fecha);
		RingList<VORoute> rutas = null;
		Retornable ret = null;

		if(!paradasCompartidas.isEmpty() && paradasCompartidas.contains(idParada))
		{
			ret = paradasCompartidas.get(idParada);
			rutas = ret.darRutas();
		}

		//Cada ruta tiene un atributo listaViajes 
		return rutas;

	}


	//Metodo auxiliar 4C
	public RojoNegro<String, Retornable> darParadasCompartidas(String fecha)
	{
		HashTableChaining<VOStop, RingList<VORoute>> rutasParadasFecha = rutasQuePasanParadasEnFecha(fecha);
		RojoNegro<String, Retornable> resp = new RojoNegro<String, Retornable>();

		if(!rutasParadasFecha.isEmpty())
		{
			Iterator<VOStop> iterator = rutasParadasFecha.keys();

			while(iterator.hasNext())
			{
				VOStop llaveActual = iterator.next();
				RingList<VORoute> rutas = rutasParadasFecha.get(llaveActual);

				if(rutas.size()>1)
				{
					Retornable ret = new Retornable(llaveActual, rutas);
					resp.put(llaveActual.id(), ret);
				}
			}
		}

		return resp;
	}


	//Punto 5C
	@Override
	public RingList<VOTrip> viajesRango(String idRuta, String horaInicio, String horaFin) 
	{
		RojoNegro arbol= new RojoNegro<>();
		int horaI = horaAsegundos(horaInicio);
		int horaF = horaAsegundos(horaFin);
		RangoHora rango = new RangoHora(horaInicio, horaFin);

		RingList viajesRuta = new RingList();
		if(!rutasChaining.isEmpty())
		{
			viajesRuta = rutasChaining.get(idRuta).darViajes();
		}
		if(!viajesRuta.isEmpty())
		{
			for (int i = 0; i < viajesRuta.size(); i++)
			{
				arbol.put(((VOTrip) viajesRuta.getElement(i)).darTiempoLlegada(), viajesRuta.getElement(i));
			}
		}
		Iterable viajes =arbol.keys(horaI, horaF);
		Iterator iterator = viajes.iterator();
		RingList rta = new RingList();
		while(iterator.hasNext())
		{
			VOTrip viajeAct = (VOTrip) iterator.next();
			rta.addFirst(viajeAct);
		}

		return rta;
	}

	public HashTableChaining darCalendarios()
	{
		return calendarios;
	}

	//------------------------------------
	// METODO MAIN
	//------------------------------------

	public static void main(String[] args) 
	{
		STSManager variable = new STSManager();


		//		long startTime3A = System.currentTimeMillis();
		//		
		//		
		//		 long endTime3A = (System.currentTimeMillis() - startTime3A);
		//		 System.out.println("El tiempo de estimar el punto 3A es: "+endTime3A+" mili segundos");


		variable.cargarGTFS();
		variable.cargarTR("20170821");
		variable.init();

		System.out.println(variable.viajesRetrasadosRuta("31324", "20170821"));


	}

}
