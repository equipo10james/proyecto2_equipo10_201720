package model.vo;

import java.util.Iterator;

import model.data_structures.ComparableExp;
import model.data_structures.RingList;

public class VOTrip implements ComparableExp<VOTrip>,Comparable
{
	private String route_id;
	private String service_id;
	private String trip_id;
	private String trip_head_sign;
	private String trip_short_name;
	private String direction_id;
	private String block_id;
	private String shape_id;
	private String wheelchair_accessible;
	private String bikes_allowed;
	
	private RingList<VOStop> listaParadas;
	private RingList<VOStopTimes> listaStopTimes;
	
	private RingList<VOShape> shapes;
	private RingList<VOCalendarDates> exceptionsCalendar;
	private VOCalendar calendario;
	
	private RingList<VOBusUpdate> updates;
	
	private RingList<VORetraso> retardos;
	private Fecha fecha;
	
	private String tiempoLlegada;
	
	
	public VOTrip(String pRoute_id, String pService_id, String pTrip_id, String pTrip_head_sign, String pTrip_short_name, String pDirection_id, String pBlock_id, String pShape_id, String pWheelchair_accessible, String pBikes_allowed  )
	{
		route_id = pRoute_id;
		service_id = pService_id;
		trip_id = pTrip_id;
		trip_head_sign = pTrip_head_sign;
		trip_short_name = pTrip_short_name;
		direction_id = pDirection_id;
		block_id = pBlock_id;
		shape_id = pShape_id;
		wheelchair_accessible = pWheelchair_accessible;
		bikes_allowed = pBikes_allowed;
		
		listaParadas = new  RingList<VOStop>();
		listaStopTimes = new RingList<VOStopTimes>();
		shapes = new RingList<VOShape>();
		calendario = null;
		exceptionsCalendar = new RingList<VOCalendarDates>();
		retardos = new RingList<VORetraso>();
		tiempoLlegada = new String();
		 updates = new RingList<VOBusUpdate>();
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String route_id() 
	{
		return route_id;
	}
	
	
	public  RingList<VOBusUpdate> darUpdatesViajes()
	{
		return updates;
	}
	
	public void agregarUpdate(VOBusUpdate update)
	{
		updates.addLast(update);
	}
	
	public RingList<VORetraso> darRetrasos()
	{
		return retardos;
	}
	public void definirRetardos(RingList<VORetraso> pretardos)
	{
		for (int i = 0; i < pretardos.size(); i++)
		{
			for (int j = i + 1; j < pretardos.size(); j++)
			{
				VORetraso retardoi = ((VORetraso)pretardos.getNode(i).getElement());
				VORetraso retardoj = ((VORetraso)pretardos.getNode(j).getElement());
				if(retardoi.darSegundosRetrasados() > retardoj.darSegundosRetrasados())
				{
					pretardos.switchWithNext(i);
				}
			}
		}
		retardos = pretardos;
	}
	
	public void definirFecha()
	{
		
	}
	public Fecha darFecha()
	{
		return fecha;
	}
	public String service_id() 
	{
		return service_id;
	}
	
	public String trip_id() 
	{
		return trip_id;
	}
	
	public String trip_head_sign() 
	{
		return trip_head_sign;
	}
	
	public String trip_short_name() 
	{
		return trip_short_name;
	}
	
	public String direction_id() 
	{
		return direction_id;
	}
	
	public String block_id() 
	{
		return block_id;
	}
	
	public String shape_id() 
	{
		return shape_id;
	}
	
	public String wheelchair_accessible() 
	{
		return wheelchair_accessible;
	}
	
	public String bikes_allowed() 
	{
		return bikes_allowed;
	}
	
	//Suponemos que paradas se van a guardar en orden 
	public void agregarParada(VOStop pParada)
	{
		listaParadas.addFirst(pParada);
	}
	
	public void agregarStopTimes(VOStopTimes pStopTime)
	{
		listaStopTimes.addFirst(pStopTime);
	}
	
	public void agregarShape(VOShape pShape)
	{
		shapes.addFirst(pShape);
	}
	
	public void agregarCalendarException(VOCalendarDates exception)
	{
		exceptionsCalendar.addLast(exception);
	}
	
	public void agregarCalendario(VOCalendar pCalendario)
	{
		calendario = pCalendario;
	}
	
	public int numParadas()
	{
	
		return listaParadas.size();
	}
	
	public RingList<VOStop> darParadas()
	{
		return listaParadas;
	}
	
	public RingList<VOStopTimes> darStopTimes()
	{
		return listaStopTimes;
	}
	
//	//Se toma que pFecha va a tener un formato AAAAMMDD.Z donde Z es la inicial del dia de la semana en español (i es miercoles)
//	public boolean verificarFecha(String pFecha)
//	{
//		boolean rangoFecha = false;
//		boolean bdia = false;
//		boolean resp = false;
//		
//		String[] info = pFecha.split(".");
//		String fecha = info[0];
//		String dia = info[1];
//		
//		if(calendario.start_date().compareTo(fecha)<0 && calendario.end_date().compareTo(fecha)>0)
//		{
//			rangoFecha = true;
//		}
//		
//		if (dia.equalsIgnoreCase("l"))
//		{
//			if(calendario.monday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("m"))
//		{
//			if(calendario.tuesday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("i"))
//		{
//			if(calendario.wednesday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("j"))
//		{
//			if(calendario.thursday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("v"))
//		{
//			if(calendario.friday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("s"))
//		{
//			if(calendario.saturday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		if (dia.equalsIgnoreCase("d"))
//		{
//			if(calendario.sunday().compareTo("1") == 0)
//			{
//				bdia = true;
//			}
//		}
//		
//		 if (rangoFecha && bdia)
//		 {
//			 resp = true;
//		 }
//		 
//		 return resp;
//	}
	
	public void cargarRetrasos(RingList<VORetraso> pRetrasos)
	{
		for (int i = 0; i < pRetrasos.size(); i++) 
		{
			retardos.addLast((VORetraso) pRetrasos.getElement(i));
		}
		
	}

	public double retardoPromedioSegundos()
	{
		double segundos = 0;
		for (int i = 0; i < retardos.size(); i++)
		{
			segundos += ((VORetraso)retardos.getElement(i)).darSegundosRetrasados();
		}
		
		double distancia = 0;
		for (int i = 0; i < shapes.size(); i++)
		{
			if(((VOShape)shapes.getElement(i)).shape_id().equals(shape_id))
			{
				distancia = Double.parseDouble(((VOShape)shapes.getElement(i)).shape_dist_traveled());
			}
		}
		return segundos/distancia;
	}
	
	public double distancia()
	{
		double distancia = 0;
		for (int i = 0; i < shapes.size(); i++)
		{
			if(((VOShape)shapes.getElement(i)).shape_id().equals(shape_id))
			{
				distancia = Double.parseDouble(((VOShape)shapes.getElement(i)).shape_dist_traveled());
			}
		}
		return distancia;
	}
	
	public VOCalendar darCalendar()
	{
		return calendario;
	}
	
	public RingList<VOCalendarDates> darExcepciones()
	{
		return exceptionsCalendar;
	}
	
	public String darTiempoLlegada()
	{
		return tiempoLlegada;
	}
	
	public void actualizarTiempoLlegada(String pTiempo)
	{
		tiempoLlegada = pTiempo;
	}
	
	public String toString ()
	{
		return trip_id;
	}
	
	private RingList<VOStop> paradasRetraso;
	
	public void ordenarParadasPorTiempoRetraso()
	{
		paradasRetraso = new RingList<VOStop>();
		
		
		for (int i = 0; i < retardos.size(); i++) 
		{
			VORetraso retrasoActual = (VORetraso) retardos.getElement(i);
			
			for (int j = 0; j < listaParadas.size(); j++) 
			{
				VOStop paradaActual = (VOStop) listaParadas.getElement(j);
				
				if(retrasoActual.parada().id().equals(paradaActual.id()))
				{
					VOStop auxiliar = paradaActual;
					auxiliar.asignarTiempoRetraso(retrasoActual.darSegundosRetrasados());
					paradasRetraso.addLast(auxiliar);
				}
				
			}
		}
		paradasRetraso.sort(1);
		
	}
	
	public int compareTo(VOTrip viaje, int i) 
	{
		if (i==1)
		{
			//Si viaje.trip_id > trip_id
			if(Integer.parseInt(trip_id)-Integer.parseInt(viaje.trip_id)<0)
			{
				return 1;
			}
			
			//Si viaje.trip_id < trip_id
			else if(Integer.parseInt(trip_id)-Integer.parseInt(viaje.trip_id)>0)
			{
				return -1;
			}
			else
				return 0;
			
		}
		
		return 0;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		 int rta = 0;
		 if (distancia()< ((VOTrip) o).distancia())
		 {
			 rta = -1;
		}
		 else if(distancia() > ((VOTrip) o).distancia())
		 {
			 rta =1;
		 }
		return rta;
	}
	
}
