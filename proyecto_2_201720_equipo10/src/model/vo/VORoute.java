package model.vo;

import model.data_structures.MaxPQ;
import model.data_structures.RingList;

/**
 * Representation of a route object
 */
public class VORoute {

	private String ruta_id;
	private String agencia_id;
	private String ruta_nombre_corto;
	private String ruta_nombre_largo;
	private String ruta_desc;
	private String ruta_tipo;
	private String ruta_url;
	private String ruta_color;
	private String ruta_text_color;
	private RingList<VOTrip> viajes;
	private MaxPQ rangosHora; //El pq se guarda deacuerdo al numero de paradas con retrasos.Toca implementar lo necesario par tener la informacion

	
	public VORoute(String pRuta_id, String pAgencia_id, String pRuta_nombre_corto, String pRuta_nombre_largo, String pRuta_desc, String pRuta_tipo, String pRuta_url, String pRuta_color, String pRuta_text_color  )
	{
		ruta_id = pRuta_id;
		agencia_id = pAgencia_id;
		ruta_nombre_corto = pRuta_nombre_corto;
		ruta_nombre_largo = pRuta_nombre_largo;
		ruta_desc = pRuta_desc;
		ruta_tipo = pRuta_tipo;
		ruta_url = pRuta_url;
		ruta_color = pRuta_color;
		ruta_text_color =pRuta_text_color;
		
		viajes = new RingList<VOTrip>();
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String id() 
	{
		return ruta_id;
	}
	
	public String agencia_id() 
	{
		return agencia_id;
	}
	
	public String nombre_corto() 
	{
		return ruta_nombre_corto;
	}
	
	public String nombre_largo() 
	{
		return ruta_nombre_largo;
	}
	
	public String desc() 
	{
		return ruta_desc;
	}
	
	public String tipo() 
	{
		return ruta_tipo;
	}
	
	public String url() 
	{
		return ruta_url;
	}
	
	public String color() 
	{
		return ruta_color;
	}
	
	public String text_color() 
	{
		return ruta_text_color;
	}
	
	public void agregarViaje(VOTrip pViaje)
	{
		viajes.addFirst(pViaje);
		
	}
	

	public void ordenarViajes()
	{
		for (int i = 1; i < viajes.size(); i++) 
		{
			for (int j = 0; j < viajes.size()-1; j++) 
			{
				if(((VOTrip) viajes.getElement(j)).trip_id().compareTo(((VOTrip) viajes.getElement(j+1)).trip_id()) > 0)
				{
					viajes.switchWithNext(j);
				}
			}
		}
	}
	public RingList<VOTrip> darViajes()
	{
		return viajes;
	}


	
//	//Se toma que pFecha va a tener un formato AAAAMMDD.Z donde Z es la inicial del dia de la semana en español (i es miercoles)
//	public boolean verificarFecha(String pFecha)
//	{
//		boolean rep = false;
//		
//		for (int i = 0; i < viajes.size(); i++) 
//		{
//			if (((VOTrip) viajes.getElement(i)).verificarFecha(pFecha) == true)
//			{
//				rep = true;
//			}
//		}
//		
//		
//		return rep;
//	}
	
	public double darPromedioRetrasosSegundos()
	{
		double sumaDEtrips = 0;
		double rta = 0;
		
		for (int i = 0; i < viajes.size(); i++)
		{
			sumaDEtrips += ((VOTrip)viajes.getElement(i)).retardoPromedioSegundos();
		}
		rta = sumaDEtrips/viajes.size();
		return rta;
	}
	public void definirRangos(RingList rangos)
	{
		for (int i = 0; i < rangos.size(); i++) 
		{
			rangosHora.insert(rangos.getElement(i));
		}
	}
	
	public String toString()
	{
		return "La ruta es "+ ruta_id + " y sus viajes son:" + viajes.toString();
	}
	public MaxPQ darRangosHora()
	{
		return rangosHora;
	}
}
