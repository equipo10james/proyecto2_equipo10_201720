package model.vo;

public class VOStopTimes 
{
	

	private String trip_id;
	private String arrival_time;
	private String departure_time;
	private String stop_id;
	private String stop_sequence;
	private String stop_headsign;
	private String pickup_type;
	private String drop_off_type;
	private String shape_dist_traveled;
	
	
	
	public VOStopTimes(String pTrip_id, String pArrival_time, String pDeparture_time, String pStop_id, String pStop_sequence, String pStop_headsign, String pPickup_type, String pDrop_off_type, String pShape_dist_traveled  )
	{
		trip_id = pTrip_id;
		arrival_time = pArrival_time;
		departure_time = pDeparture_time;
		stop_id = pStop_id;
		stop_sequence = pStop_sequence;
		stop_headsign = pStop_headsign;
		pickup_type = pPickup_type;
		drop_off_type = pDrop_off_type;
		shape_dist_traveled = pShape_dist_traveled;
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String trip_id() 
	{
		return trip_id;
	}
	
	public String arrival_time() 
	{
		return arrival_time;
	}
	
	public String departure_time() 
	{
		return departure_time;
	}
	
	public String stop_id() 
	{
		return stop_id;
	}
	
	public String stop_sequence() 
	{
		return stop_sequence;
	}
	
	public String stop_headsign() 
	{
		return stop_headsign;
	}
	
	public String pickup_type() 
	{
		return pickup_type;
	}
	
	public String drop_off_type() 
	{
		return drop_off_type;
	}
	
	public String shape_dist_traveled() 
	{
		return shape_dist_traveled;
	}
	
	public String toString()
	{
		return trip_id ;
	}
	
}
