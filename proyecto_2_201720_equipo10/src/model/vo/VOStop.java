package model.vo;

import model.data_structures.ComparableExp;
import model.data_structures.RingList;

/**
 * Representation of a Stop object
 */
public class VOStop implements ComparableExp<VOStop>
{


	private String stop_id;
	private String stop_codigo;
	private String stop_nombre;
	private String stop_desc;
	private String stop_lat;
	private String stop_lon;
	private String zone_id;
	private String stop_url;
	private String location_type;
	private String parent_station;
	private RingList<VORoute> rutas;
	private RingList<String> viajes;
	private boolean huboRetraso;
	
	private int cantidadRetrasos;
	private double tiempoRetraso;
	private boolean marcador;
	
	public VOStop(String pStop_id, String pStop_codigo, String pStop_nombre, String pStop_desc, String pStop_lat, String pStop_lon, String pZone_id, String pStop_url, String pLocation_type, String pParent_station  )
	{
		stop_id = pStop_id;
		stop_codigo = pStop_codigo;
		stop_nombre = pStop_nombre;
		stop_desc = pStop_desc;
		stop_lat = pStop_lat;
		stop_lon = pStop_lon;
		zone_id = pZone_id;
		stop_url = pStop_url;
		location_type = pLocation_type;
		parent_station = pParent_station;
		
		viajes = new RingList<String>();
		
		tiempoRetraso = 0;
		cantidadRetrasos = 0;
		huboRetraso = false;
		marcador = false;
	}

	public void agregarNumRetraso()
	{
		cantidadRetrasos += 1;
	}
	
	public int darCantidaRetrasos()
	{
		return cantidadRetrasos;
	}
	
	public void asignarTiempoRetraso(double tiempo)
	{
		tiempoRetraso = tiempo;
	}
	
	public double darTiempoRetraso()
	{
		return tiempoRetraso;
	}
	
	public boolean huboRetraso()
	{
		return huboRetraso;
	}
	
	/**
	 * @return id - Route's id number
	 */
	public String id() 
	{
		return stop_id;
	}
	
	public String codigo() 
	{
		return stop_codigo;
	}
	
	public String nombre() 
	{
		return stop_nombre;
	}
	
	public String desc() 
	{
		return stop_desc;
	}
	
	public String lat() 
	{
		return stop_lat;
	}
	
	public String lon() 
	{
		return stop_lon;
	}
	
	public String zone_id() 
	{
		return zone_id;
	}
	
	public String url() 
	{
		return stop_url;
	}
	
	public String location_type() 
	{
		return location_type;
	}
	
	public String parent_station() 
	{
		return parent_station;
	}
	
	public void agregarRuta(VORoute ruta)
	{
		rutas.addFirst(ruta);
	}
	
	public RingList<VORoute> darRutas()
	{
		return rutas;
	}
	
	public void agregarViaje(String viaje)
	{
		
		viajes.addFirst(viaje);
	}
	
	public RingList<String> darViajes()
	{
		return viajes;
	}

	public void marcarPaso()
	{
		marcador = true;
	}

	public boolean darMarcador()
	{
		return marcador;
	}
	
	public String toString ()
	{
		return "La parada tiene es: "+ nombre() + " con id: "+id();
	}
	
	public int compareTo(VOStop parada, int i) 
	{
		if (i==1)
		{
			
			if(tiempoRetraso-parada.darTiempoRetraso()<0)
			{
				return 1;
			}
			
			else if(tiempoRetraso-parada.darTiempoRetraso()>0)
			{
				return -1;
			}
			else
				return 0;
			
		}
		
		if(i==2)
		{
			return cantidadRetrasos-parada.darCantidaRetrasos();
		}
		
		return 0;
	}
	
	public int compareTo(VOStop parada)
	{
		return cantidadRetrasos-parada.darCantidaRetrasos();
	}
	
}
