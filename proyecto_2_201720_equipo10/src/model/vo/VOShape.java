package model.vo;

public class VOShape 
{

	private String shape_id;
	private String shape_pt_lat;
	private String shape_pt_lon;
	private String shape_pt_sequence;
	private String shape_dist_traveled;
	
	public VOShape(String pshape_id, String pshape_pt_lat, String pshape_pt_lon, String pshape_pt_sequence, String pshape_dist_traveled)
	{
		shape_id = pshape_id;
		shape_pt_lat = pshape_pt_lat;
		shape_pt_lon = pshape_pt_lon;
		shape_pt_sequence = pshape_pt_sequence;
		shape_dist_traveled = pshape_dist_traveled;
	}
	
	public String shape_id()
	{
		return shape_id;
	}
	
	public String shape_pt_lat()
	{
		return shape_pt_lat;
	}
	
	public String shape_pt_lon()
	{
		return shape_pt_lon;
	}
	
	public String shape_pt_sequence()
	{
		return shape_pt_sequence;
	}
	
	public String shape_dist_traveled()
	{
		return shape_dist_traveled;
	}
	
	public String toString()
	{
		return shape_id;
	}
	
}
