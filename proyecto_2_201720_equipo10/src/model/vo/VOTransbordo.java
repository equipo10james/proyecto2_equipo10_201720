package model.vo;

import model.data_structures.ComparableExp;
import model.data_structures.RingList;

public class VOTransbordo implements ComparableExp<VOTransbordo>
{
	
	private RingList<VOStop> paradas;
	
	private int tiempoTotal; 
	
	
	public VOTransbordo ()
	{
		tiempoTotal = 0;
		
		paradas = new RingList<VOStop>();
	}
	

	public RingList<VOStop> darParadas()
	{
		return paradas;
	}
	
	public void agregarParada(VOStop stop)
	{
		paradas.addLast(stop);
	}
	
	public void agregarParadas(RingList<VOStop> stops)
	{
		for (int i = 0; i < stops.size(); i++) 
		{
			paradas.addLast((VOStop) stops.getElement(i));
		}
	}

	public void actualizarTiempo(int ptiempo)
	{
		tiempoTotal += ptiempo;
	}
	
	public int darTiempo()
	{
		return tiempoTotal;
	}
	

	public int compareTo(VOTransbordo transbordo, int i) 
	{
		if (i==1)
		{
			return (int) (paradas.size() - transbordo.paradas.size());
		}
		
		return 0;
	}
	
	public String toString()
	{
		return paradas.toString();
	}
	
}
