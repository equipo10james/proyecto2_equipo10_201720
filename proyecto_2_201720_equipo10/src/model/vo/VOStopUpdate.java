package model.vo;

public class VOStopUpdate 
{

	private String routeNo;
	private String routeName;
	private String direction;
	private String routeMap;
	private String schedules;
	
	
	public VOStopUpdate(String prouteNo, String prouteName, String pdirection, String prouteMap, String pschedules )
	{
		routeNo = prouteNo;
		routeName = prouteName;
		direction = pdirection;
		routeMap = prouteMap;
		schedules = pschedules;
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String routeNo() 
	{
		return routeNo;
	}
	
	public String routeName() 
	{
		return routeName;
	}
	
	public String direction() 
	{
		return direction;
	}
	
	public String routeMap() 
	{
		return routeMap;
	}
	
	public String schedules() 
	{
		return schedules;
	}
}
