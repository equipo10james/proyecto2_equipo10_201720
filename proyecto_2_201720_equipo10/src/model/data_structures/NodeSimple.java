package model.data_structures;

public class NodeSimple<E>
{
	private E elemento;
	private NodeSimple<E> next;
	
	public NodeSimple(E e, NodeSimple<E> n)
	{
		elemento = e;
		next = n;
	}
	
	public E getElement()
	{
		return elemento;
	}
	

	public NodeSimple<E> getNext()
	{
		return next;
	}

	public void setNext(NodeSimple<E> n)
	{
		next = n;
	}
}
