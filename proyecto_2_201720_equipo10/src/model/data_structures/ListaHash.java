package model.data_structures;

public class ListaHash <Key, Value>
{
	 public class Node<Key, Value> {

	        private Key key;
	        private Value val;
	        private Node next;

	        public Node(Key key, Value val, Node next)  {
	            this.key  = key;
	            this.val  = val;
	            this.next = next;
	        }
	        
	        public Value darValor()
	        {
	        	Value x = null;
	        	if(val != null)
	        	{
	        		x = val;
	        	}
	        	return x;
	        }
	        
	        public Key key()
	        {
	        	Key x = null;
	        	if(key != null)
	        	{
	        		x = key;
	        	}
	        	return x;
	        }
	        
	        public Node next()
	        {
	        	Node x = null;
	        	if(next != null)
	        	{
	        		x =next;
	        	}
	        	
	        	return x;
	        }
	    }
	 
	 private int size;
	 private Node first;
	 
	 public ListaHash()
	 {
		 
	 }
	 
	 public Node first()
	 {
		 return first;
	 }
	 public int size()
	 {
		 return size;
		 
	 }
	 public boolean isEmpty() 
	 {
	        return size() == 0;
	 }
	 
	 public Value get(Key key)
	 {
	        for (Node x = first; x != null; x = x.next()) {
	            if (key.equals(x.key()))
	                return (Value) x.darValor();
	        }
	        return null;
	 }
	 
	    public boolean contains(Key key) {
	        return get(key) != null;
	    }

	 
	 private Node delete(Node x, Key key) 
	 {
	        if (x == null) return null;
	        if (key.equals(x.key)) {
	            size --;
	            return x.next;
	        }
	        x.next = delete(x.next, key);
	        return x;
	 }
	 
	 public void delete(Key key) {
	        first = delete(first, key);
	    }
	 
	 
	 public void put(Key key, Value val) {
	        if (val == null) {
	            delete(key);
	            return;
	        }

	        for (Node x = first; x != null; x = x.next) {
	            if (key.equals(x.key)) {
	                x.val = val;
	                return;
	            }
	        }
	        first = new Node(key, val, first);
	        size++;
	    }

	 
	 public Iterable<Key> keys()
	 	{
	         RingList<Key> lista = new RingList<Key>();
	        for (Node x = first; x != null; x = x.next)
	        	lista.addLast((Key) x.key);
	        return lista;
	    }

}
