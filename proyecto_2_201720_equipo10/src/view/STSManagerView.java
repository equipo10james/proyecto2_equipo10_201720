package view;

import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOTrip;


public class STSManagerView 
{

	/**
	 * Main
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while (!fin) 
		{
			printMenu();

			int option = sc.nextInt();

			switch (option) 
			{

			//1C
			case 1:

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				/**
				 * Metodo de Controller
				 */
				Controller.ITScargarGTFS();

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime) / (1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  " + ((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + " MB");

				
				
				break;

			//1C
			case 2:
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (AAAAMMDD) \n Esta fecha se utilizara para los otros metodos.");

				//Fecha deseada
				String fechaCase2 = sc.next();

				// TODO Cargar informacion tiempo real
				/**
				 * Metodo de Controller
				 * Carga la informacion en tiempo real de los buses en para fecha determinada.
				 * @param fecha
				 */ 
				Controller.ITScargarTR(fechaCase2);
				Controller.ITSInit();

				break;
				
			//1A
			case 3:

				//id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase3 = sc.next();

				//TODO  REQUERIMIENTO 1A 
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return RingList<VOViaje>
				 */

				System.out.println(Controller.ITSviajesRetrasadosRuta(idRouteCase3));

				break;

			//2A
			case 4:
				System.out.println("Ingrese el n que desea:" );
				String n2= sc.next();
				int n = Integer.parseInt(n2);

				//TODO  REQUERIMIENTO 2A 
				/**
				 * Metodo de Controller
				 * @param int n
				 * @return RingList<VOParada>
				 */

				System.out.println(Controller.ITSparadasRetrasadasFecha(n));

				break;

			//3A
			case 5:

				//id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase5 = sc.next();

				//TODO  REQUERIMIENTO 3A 
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return RingList<VOTransbordo>
				 */
				System.out.println(Controller.ITStransbordosRuta(idRouteCase5));

				break;

			//1B
			case 6:

				//Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase6 = sc.next();

				//TODO  REQUERIMIENTO 1B 
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return RingList<VOViaje>
				 */
				System.out.println(Controller.ITSviajesRetrasoTotalRuta(idRutaCase6));

				break;

			//2B
			case 7:
				//Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase7 = sc.next();

				// TODO REQUERIMIENTO 2B
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @return VORangoHora
				 */
				System.out.println(Controller.ITSretardoHoraRuta(idRutaCase7));

				break;

			//3B
			case 8:

				//Id parada origen
				System.out.println("Ingrese el id de la parada origen");
				String idOrigen8 = sc.next();

				//Id parada destino
				System.out.println("Ingrese el id de la parada destino");
				String idDestino8 = sc.next();

				//Hora de inicio
				System.out.println("Ingrese la hora inicial en formato 4:50:00");
				String horaInicio8 = sc.next();

				//Hora de fin
				System.out.println("Ingrese la hora final en formato 4:50:00");
				String horaFin8 = sc.next();
				//TODO REQUERIMIENTO 3B
				/**
				 * Metodo de Controller
				 * @param String idOrigen
				 * @param String idDestino
				 * @param String horaInicio
				 * @param String horaFin
				 * @return RingList<VOViaje>
				 */
				System.out.println(Controller.ITSbuscarViajesParadas(idOrigen8, idDestino8, horaInicio8, horaFin8));
				break;

			//2C
			case 9:

				//n 
				System.out.println("Ingrese el n que desea:" );
				String n1 = sc.next();
				int n3 = Integer.parseInt(n1);
				//TODO REQUERIMIENTO 2C
				/**
				 * Metodo de Controller
				 * @param int n
				 * @return RingList<VOViaje>
				 */
				System.out.println(Controller.ITSviajesMayorDistancia(n3));

				break;

			//3C
			case 10:

				//Id viaje
				System.out.println("Ingrese el id del viaje deseado:");
				String idViajeCase10 = sc.next();

				//TODO REQUERIMIENTO 3C
				/**
				 * Metodo de Controller
				 * @param String idViaje
				 * @return RojoNegro<String, VORetraso>
				 */
				System.out.println(Controller.retardosViaje(idViajeCase10));
				break;

			//4C
			case 11:

				//Id parada
				System.out.println("Ingerse el id de la parada deseada");
				String idParada11 = sc.next();

				//TODO REQUERIMIENTO 4C
				/**
				 * Metodo de Controller
				 * @return RingList<VORoute>
				 */
				System.out.println(Controller.ITSParadasCompartidas(idParada11));

				break;

			//5C
			case 12:

				//Ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String rutaCase12 = sc.next();

				//HoraInicio
				System.out.println("Ingrese la hora inicial en formato 23:50:00 ");
				String horaInicio12 = sc.next();

				//HoraFinal
				System.out.println("Ingrese la hora final en formato 23:50:00 ");
				String horaFinal12 = sc.next();

				//TODO REQUERIMIENTO 5C
				/**
				 * Metodo de Controller
				 * @param String idRuta
				 * @param String horaInicio
				 * @param String horaFin
				 * @return RingList<VOTrip>
				 */
				System.out.println(Controller.viajesRango(rutaCase12, horaInicio12, horaFinal12));

				break;


				//SALIR
			case 13:

				fin = true;
				sc.close();
				break;

			}
		}
	}

	/**
	 * Menu
	 */

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar la informacion estatica necesaria para la operacion del sistema");
		System.out.println("2. Cargar la informacion en tiempo real de los buses dada una fecha y la información extra \n");

		System.out.println("Parte A:");
		System.out.println("3.(1A) Dada una ruta  y una fecha,  identificar todos los viajes en los que hubo un retardo en al menos una parada.");
		System.out.println("4.(2A) Identificar  las n paradas en las que hubo más retardos en una fecha dada, teniendo en cuenta todas las rutas que utilizan esa parada.");
		System.out.println("5.(3A) Para una ruta dada en una fecha dada, retornar una lista ordenada  (por tiempo total de viaje) de todos los transbordos posibles, a  otras rutas, a partir de las paradas de dicha ruta.  \n");


		System.out.println("Parte B: ");
		System.out.println("6.(1B) Dada una ruta y una fecha, identificar todos los  viajes en los que después  de un retardo, todas las paradas siguientes tuvieron retardo. Retornar lista ordenada descendentemente por id de viaje.");
		System.out.println("7.(2B) Identificar  la franja de hora entera en la que hubo más paradas con retardos, para una ruta determinada en una fecha dada.");
		System.out.println("8.(3B) Dados los ids de una parada de origen y una parada de destino, buscar los viajes de rutas de bus para ir del origen al destino, en una fecha y franja de horario.  \n");


		System.out.println("Parte C:");
		System.out.println("9. (2C) Identificar  los n viajes que recorren más distancia  en una fecha dada, retornando una lista ordenada por esta distancia.");
		System.out.println("10.(3C) Retornar un árbol  balanceado de retardos de un viaje (ordenado por tiempo de retardo), dado su  identificador y una fecha  dada. ");
		System.out.println("11.(4C) Dada una parada y una fecha, responder si dicha parada es compartida o no en dicha fecha.");
		System.out.println("12.(5C) Dada una ruta, dar todos los viajes (con sus respectivas paradas) que realizaron paradas en un rango de tiempo dado. \n");
		System.out.println("13. Salir.\n");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}