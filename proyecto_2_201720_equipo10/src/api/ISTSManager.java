package api;

import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.vo.RangoHora;
import model.vo.Retornable;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOTransbordo;
import model.vo.VOTrip;

public interface ISTSManager 
{
	/**
	* Carga toda la informacion estatica necesaria para la operacion del sistema.
	*(Archivo de rutas, viajes, paradas, etc.)
	*/
	public void cargarGTFS( );

	/**
	* Carga la informacion en tiempo real de los buses para fecha determinada.
	* @param fecha
	*/
	public void cargarTR(String fecha); 
	
	/**
	* Retorna una lista de viajes (ordenada por el id), que tuvieron retardo en almenos una parada. 
	* @param idRuta
	* @param fecha
	* @return RingList de VOTrip.
	*/
	public RingList<VOTrip> viajesRetrasadosRuta(String idRuta, String fecha);
	
	/**
	* Retorna una lista de las n paradas que tuvieron mas retardos en la fecha que llega por parametro.
	* @param fecha.
	* @param n
	* @return RingList de VOStop.
	*/
	public RingList<VOStop> paradasRetrasadasFecha( int n, String fecha);
	
	/**
	* Retorna una lista con todos los transbordos posible a partir de una ruta.
	* @param idRuta
	* @param fecha
	* @return RingList de VOTransbordo
	*/
	public RingList<VOTransbordo> ITStransbordosRuta(String idRuta, String fecha); 
	
	/**
	* Retornar una lista con todos los viajes en los que despues de un retardo, todas las 
	* paradas siguientes tuvieron retardo, para una ruta especifica en una fecha especifica. 
	* Se debe retornar una lista ordenada por el id del viaje, y la localizacion de las 
	* paradas, de mayor a menor en tiempo de retardo.
	* @param idRuta
	* @param fecha
	* @return RingList de VOTrip
	*/
	public RingList<VOTrip> viajesRetrasoTotalRuta(String idRuta, String fecha) ;
	
	/**
	* Retorna la hora entera de más retardos de la ruta incluyendo la lista de todos 
	* los viajes con retardo durante esta hora.
	* @param idRuta
	* @param Fecha
	* @return RangoHora
	*/
	public RangoHora ITSretardoHoraRuta (String idRuta, String Fecha );
	
	/**
	* Retorna una lista con los viajes para ir de la parada de inicio a la parada 
	* final en una fecha y franja horaria determinada.
	* @param idOrigen
	* @param idDestino
	* @param fecha
	* @param horaInicio
	* @param horaFin
	* @return RingList de VOTrip
	*/
	public RingList<VOTrip> buscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin); 
	
	/**
	* Inicializa estrucuturas de datos y carga toda la información necesaria
	*/
	public void init();
	
	/**
	*  Retorna los n viajes que recorrieron más distancia en la fecha dada por parámetro.
	* @param n
	* @param fecha
	* @return
	*/
	public RingList<VOTrip> viajesMayorDistancia (int n, String fecha);
	
	/**
	* Retorna un arbol con los retardos de un viaje con id idViaje dado por parametro 
	* en la fecha dada.
	* @param idViaje
	* @param fecha
	* @return RojoNegro<String, VORetraso>
	*/
	public RojoNegro<String, VORetraso> retardosViaje (String fecha, String idViaje);
	
	/**
	* Retorna una lista con todas las paradas del sistema que son compartidas 
	* por mas de una ruta en una fecha determinada.
	* @param fecha
	* @return RingList de VORoute
	*/
	public RingList<VORoute> paradaCompartida (String fecha, String idParada);
	
	/**
	* Retorna los viajes de una ruta dada, que pararon en el rango de tiempo dado.
	* @param idRuta
	* @param horaInicio
	* @param horaFin
	* @return RingList de VOTrip
	*/
	public RingList<VOTrip> viajesRango (String idRuta, String horaInicio, String horaFin );
}
