Santiago Rangel 201632011
Daniela Gonzalez, 201631227

1. En relación entre el uml y la diseño de mundo que planeamos: 
La clase principal es el STSManager que tiene como función cargar todos los datos,
guardarlos y realizar funciones con estos datos. Esta clase tiene como atributos
estructuras que pueden ser RingList, MaxPQ, HashTables o RojoNegro. Estos nodos tiene como atributo un elemento que van a guardar. Este elemento puede ser cualquier tipo de VO. Cada uno de estos VO tiene sus propios atributos.

En cuanto a la lectura de los dos archivos de tipo GSON, se implementó el método para la lectura del archivo que actualiza los buses y su estado. Este mismo código y esquema es el que se va a utilizar para leer el otro archivo solo que utilizando <VOStopUpdate> en vez de <VOBusUpdate>.

Para los archivos CSV, se implementaron métodos para la lecturas específica de archivos de rutas, viajes, stoptimes y paradas. Estos son los modelos para leer los archivos CSV como por ejemplo feed_info,.. entre otros. Se utilizó BufferReader ya que todos se tratan de archivos CSV simples donde no hay comas en los datos del archivo. 

Se crearon las signaturas de todos los métodos que se van a usar así como sus clases tal y como figuran en el diagrama de UML disponible en la carpeta de docs. 

2. Orden de complejidad

1A: O(n) Encuentra en O(1) la ruta con el id dado por parámetro. Se recorren todos los viajes de dicha ruta y para cada uno se verifica si en la fecha dada por parámetro, el tamaño de retrasos es más grande de 1, si es así entonces se agrega a la lista respuesta que se retorna. 

2A: O(n) Se recorre el heap de las paradas cuya prioridad es el numero de retrasos, si esa parada y ese retraso se dio en la fecha dada por parámetro, entonces dicha parada se agrega de última a la lista de respuesta que se retorna para conservar el orden.

3A: O(n^2) La ruta se encuentra en O(1) pero después es necesario recorrer las paradas de dicha ruta y para cada una verifica si solo pasa una ruta por dicha parada o más de una. En caso de ser el último caso toca recorrer todas las rutas de la parada para ver todas las paradas posibles de transbordo.

1B Necesito sacar los viajes de acuedo a una ruta y una fecha.
 Necesito los retardos. Las paradas deberian tener un boolean que diga si tiene retardo o no.
(Dice que se deben almacenar las rutas en una hash de probing)

Una ruta debe tener una lista de trips. De esta manera se simplifica mucho el problema.
Tambien cada viaje debe tener una lisrta con sus paradas.
O1 + On(Muy reducido)

2B Se necesita la clase de rango hora. 
Toca sacar el rango de hora con mas retardos en una ruta y fecha dada. 
Tocaria tener como atributo de los rangos de hora una lista de retrasos.
(Las rutas deben estar almacenadas en una tabbla de hash con chaining,
 y que por cada ruta debe haber una cola de prioridad de rangos de hora? no entendi)


Crear una clase de fecha que tenga una lista con retardos(Y todo lo que se le quiera agregar)
Se necesita que por cada ruta haya una cola de prioridad de los rangos de hora donde la ruta toma lugar(la prioridad debe ser el numero de retardos en la franja de hora)
On

De acuerdo a la ruta, sacar del hash la ruta. Luego eso da un PQ de rangos de hora. 
Quitar los rangos de hora que no esten en la fecha dada. Ahora dar el de mayor prioridad.

3B Deacuerdo a una fecha, un rango de hora, una parada origen y una parada destino dar los viajes que lo llevenn
Se debe dar el id de la ruta, el del viaje y el el	tiempo	del	viaje	en	
la	parada	origen	y	el	tiempo	del	viaje	en	la	parada	destino.?????
toca comparar toiempos


De acuerdo a las 2 paradas se sacan las dos de la yabla de hash. Cada parada tiene su lista de viajes. 
Recorro la lista de viajes de origen. 
Para cada viaje de esos busco su alguna de sus paradas es la final y si lo es se agrega a una lista de respuesta.
Luego se filtra esta lista de acuerdo a la fecha y rango de hora.

Otra solucion puede ser si tiene una clase fecha en una tabla de hash. La fecha tiene una lista de viajes.
 Recorro todos los viajes donde por cada uno miro si sus paradas tiene origen y luego la destino. Luego se filtra por rango de hora.



1C: La complejidad de este método es muy falta ya que se trata de todos los métodos que cargan la información inicial necesaria para realizar este proyecto. En cada uno de dichos métodos auxiliarles hay recorridos totales lo que aumenta rápidamente la complejidad del método. 

2C
Asumo que tengo un PQ de viajes donde la prioridad es la distancia recorrida es la prioridad. Es tan solo llamar a  la PQ y dar los N terminos.



3C: O(n) El viaje se encuentra en O(1) y se recorren los retrasos de dicho viaje y para cada retraso se verifica si esta en la fecha dada por parámetro. De ser así, el retraso se agrega al árbol que se va a retornar. 


5C: O(n) Después de encontrar la ruta en O(n) es necesario recorrer todos los viajes de dicho viaje para verificar que se encuentra en el rango de horas dado. De ser así, este viaje se agrega al árbol que se va a retornar. 


El tiempo de los métodos al cargar es de alrededor de 6000 milisegundos. Esto es mucho mejor al tiempo del proyecto 1, teniendo en cuenta que en el anterior ningún método se probó, ni se cargo la información. Teniendo esto es cuenta no se realizó el comparativo de tiempos de ejecución ya que no había con que comparar por lo que se asumió que un tiempo de orden de los segundos es una gran mejora en el programa. 
